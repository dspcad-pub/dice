/*******************************************************************
@ddblock_begin copyright 

Copyright (c) 1997-2018
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright 
********************************************************************/

/*******************************************************************************
Utilities for error handling.
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "verbose.h"


int verbose_verbosity = VERBOSITY_QUIET;
char *verbose_file = NULL;
FILE *verbose_fp = NULL;


void verbose_set_verbosity(int verbosity){
	verbose_verbosity = verbosity;
}

int verbose_get_verbosity(void){
  return verbose_verbosity;
}


void verbose_set_outfile(char *file){
	
	if(file == NULL){
		error_warn("Verbose error : the output file is not set \
		correctly!\n");
		return;
	}
	
	/*Close the file pointer if it's not NULL*/
	verbose_close_file();
	
	verbose_file = file;
	
	/* Check whether the output is stdout or stderr */
	if(strcmp(verbose_file, "stdout") == 0){
		verbose_fp = stdout;
	}
	else if(strcmp(verbose_file, "stderr") == 0){
		verbose_fp = stderr;
	}
	else{
		verbose_fp = fopen(verbose_file, "a");
	}
	
	printf("Verbose : verbose output file set\n");
	
	return;
}

char* verbose_get_file(void){
	return verbose_file;
}

void verbose_close_file(void){
	if((verbose_file != NULL) && 
	   (strcmp(verbose_file, "stdout") != 0) &&
	   (strcmp(verbose_file, "stderr") != 0)){
		
		fclose(verbose_fp);
		verbose_file = NULL;
	}
	return;
}


/* verbose print : print if verbosity level higher than VERBOSE_NORM */
void verbose_norm(const char *fmt, ...){
	
	if(verbose_verbosity >= VERBOSITY_NORM){
		
		va_list args;
		
		if(verbose_file == NULL){
		error_warn("Verbose error : the output file is not set \
		correctly!\n");
		return;
		}
		
		va_start(args, fmt);
		vfprintf(verbose_fp, fmt, args);
		va_end(args);
		return;
	}
	else{
		return;
	}
}


/* verbose print : print if verbosity level higher than VERBOSE_INFO */
void verbose_info(const char *fmt, ...){
	
	if(verbose_verbosity >= VERBOSITY_INFO){
		
		va_list args;
		
		if(verbose_file == NULL){
		error_warn("Verbose error : the output file is not set \
		correctly!\n");
		return;
		}
		
		va_start(args, fmt);
		vfprintf(verbose_fp, fmt, args);
		va_end(args);
		return;
	}
	else{
		return;
	}
}


/* verbose print : print if verbosity level higher than VERBOSE_VERY */
void verbose_very(const char *fmt, ...){
	
	if(verbose_verbosity >= VERBOSITY_VERY){
		
		va_list args;
		
		if(verbose_file == NULL){
		error_warn("Verbose error : the output file is not set \
		correctly!\n");
		return;
		}
		
		va_start(args, fmt);
		vfprintf(verbose_fp, fmt, args);
		va_end(args);
		return;
	}
	else{
		return;
	}
}


/* verbose print : print if verbosity level higher than VERBOSE_DEBUG */
void verbose_debug(const char *fmt, ...){
	
	if(verbose_verbosity >= VERBOSITY_DEBUG){
		
		va_list args;
		
		if(verbose_file == NULL){
		error_warn("Verbose error : the output file is not set \
		correctly!\n");
		return;
		}
		
		va_start(args, fmt);
		vfprintf(verbose_fp, fmt, args);
		va_end(args);
		return;
	}
	else{
		return;
	}
}

