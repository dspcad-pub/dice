Verilog language utilities for DICELANG.

The current version of dlv (DICELANG-verilog) is designed to support the ModelSim toolset. More
specifically, one needs to have the vlog, vmap, and vlib commands enabled
for all of the existing dlv utilities to work. 

The utilities available in dlv maybe useful as templates or
starting points for developing support for other
Verilog/HDL tools as extensions or plug-ins to DICE.
