demo for dlv setup with vivado

To startup the tcl command line mode, one needs to do the follow before
compiling the source and testbench code:
./<installation_path>/2019.2/bin/vivado -mode tcl

The installation_path is the path to the directory "Vivado"
