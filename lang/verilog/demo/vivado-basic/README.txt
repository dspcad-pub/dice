----------------------------------------------------------------------------
@ddblock_begin copyright
----------------------------------------------------------------------------
Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
----------------------------------------------------------------------------
@ddblock_end copyright
---------------------------------------------------------------------------

Demo for 1-bit full adder simulation with Vivado command line environment.

The src/ directory contains top level modules needed for the 1-bit full adder 
implementation in Verilog. To compile the verilog source code, one can run 
./makeme command to compile the source code.

The test/ directory contains test demo for the 1-bit full adder implementation.
In test/test01/ directory, it contains testbench for the 1-bit full adder, 
which contains the simulation inputs for the full adder. Compilation log is 
kept in transcript file.

One can run command dxtest from test/ directory to invoke unit tests for the 
designed test cases. Feel free to add more test cases to the demo as test01/ 
is only there to showcase the test case directory structure.

The test results will give clear indiciation on how many tests have been run, 
how many of them failed, and how many error output mismatches there are. For
this demo, only 1 test has been added for demoing purpose.
