#!/bin/bash
#############################################################################
# @ddblock_begin copyright 
#
# Copyright (c) 1997-2020
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
#
# Permission is hereby granted, without written agreement and without
# license or royalty fees, to use, copy, modify, and distribute this
# software and its documentation for any purpose, provided that the above
# copyright notice and the following two paragraphs appear in all copies
# of this software.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
#
# @ddblock_end copyright 
#############################################################################

# Usage: source dlvmakeme <no arguments>

# This script needs to be sourced.

#############################################################################
# Load the dlv project configuration
#############################################################################

# Clear out any previous configuration. This helps in avoiding
# hard-to-track bugs due to settings that creep in from previously
# processed projects.

dlvprojreset

source ./dlvconfig
if [ ! -n "$dlvbindir" ]; then
    dlvbindir=../bin/
fi

echo "dlvmakeme: start"

if [ -n "$dlvverbose" ]; then
    echo dlvmakeme, displaying project configuration:
    dlvprojshow 
fi

#############################################################################
# Figure out which build command to be executed
#############################################################################
if [ -z "$dlv_build_command" ]; then
    # Modelsim is used by default
    source "$UXDLV/scripts/dlv_build_modelsim"
else
    if [ -n "$dlvverbose" ]; then
        echo dlvmakeme, executing: $dlv_build_command
    fi
	source "$dlv_build_command"
    if [ $? -ne 0 ]; then
        echo dlvmakeme: error encountered when executing ...
        echo build command \"$dlv_build_command\"
    fi
fi

echo "dlvmakeme: complete"


