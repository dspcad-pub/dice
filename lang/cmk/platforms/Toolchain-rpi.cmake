# This file is adapted from an example retrieved from
# \url{https://medium.com/@au42/
# the-useful-raspberrypi-cross-compile-guide-ea56054de187}
# on Jan 21, 2021.

# This file specifies compilers, linkers, and libraries
# to build a Raspberry Pi targeted project. It is useful for cross-compilation
# of projects targeted to the Raspberry Pi.

# Define our host system
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)
# Define the cross compiler locations
SET(CPATH arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc)
SET(PPATH arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-g++)
SET(RPATH arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot)
SET(CMAKE_C_COMPILER $ENV{UXRASPITOOLS}/${CPATH})
SET(CMAKE_CXX_COMPILER $ENV{UXRASPITOOLS}/${PPATH})
# Define the sysroot path for the RaspberryPi distribution in our tools folder
SET(CMAKE_FIND_ROOT_PATH $ENV{UXRASPITOOLS}/${RPATH})
# Use our definitions for compiler tools
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# Search for libraries and headers in the target directories only
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
add_definitions(-Wall -lstdc++)

