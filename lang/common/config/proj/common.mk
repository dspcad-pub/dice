#############################################################################
# @ddblock_begin copyright
#
# Copyright (c) 1997-2018
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
# All rights reserved.

# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# 
# @ddblock_end copyright
#############################################################################

# 
# Common dicelang makefile definitions across different languages.
#

# Note that high level language-specific configurations are maintained
# here because utilities and other features for one dicelang language
# package may be developed in terms of one or more different languages.

# @author Shuvra S. Bhattacharyya based on a file by Nitin Chandrachoodan. 

# Define the standard variables
SHELL	= bash

# Compiler for ANSI C 
CC	= gcc


# Compiler for C++
CXX	= g++

# Utilities for C libary development
AR	= ar
RANLIB	= ranlib

# Compiler for Java
JAVAC = javac

# Utilities for Java archive development
JAR = jar

# Java documentation for Java
JAVADOC = javadoc

# Configure the shell that is used by make
SHELL=$(DLX_MAKESHELL)
export SHELL

