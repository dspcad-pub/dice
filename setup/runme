#!/usr/bin/env bash

################################################################################
# @ddblock_begin copyright
#
# Copyright (c) 1997-2018
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
# All rights reserved.

# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# 
# @ddblock_end copyright
################################################################################

################################################################################
# This script is to set up DICE in dicepub 
# repository. 
# 
# Usage format 1: <path to DICE installation>/setup/runme
# Usage format 2: <path to DICE installation>/setup/runme nobuild
#
# If $DXUSER is not set, the default setting of
# DXUSER="$HOME/dspcad_user/dice_user" is used.
#
################################################################################

# Export definitions to sub-processes for dice setup

lxprog="dice-setup-runme"

echo $lxprog: Starting to set up DICE ...
echo ----------------------------
echo $lxprog: Using DICE setup script:
echo $BASH_SOURCE

# Configure the package variable, which stores the path of the package
# installation. This path is derived as the parent of the directory that
# stores this runme script. 
lxsetupdir=`dirname "$BASH_SOURCE"`
pushd .
cd "$lxsetupdir/.."
if [ $? -ne 0 ]; then
    echo $lxprog error: could not find package installation directory
    popd
    exit 1
fi
# Capture the path in a standard format
export UXDICE=`pwd -P`
popd
echo $lxprog: UXDICE "is set to $UXDICE"

# Set up the dice user directory
if [ -z "$DXUSER" ]; then
    DXUSER="$HOME/dspcad_user/dice_user"
fi
mkdir -p "$DXUSER"
if [ $? -ne 0 ]; then
    echo $lxprog error: could not create/access dice user directory ---
    echo directory path: \"$DXUSER\"
    exit 1
fi

# Perform core setup process for DICE
pushd "$DXUSER"
cd ..
if [ $# -eq 1 ]; then
    if [ "$1" = "nobuild" ]; then
        "$UXDICE/setup/setup" "nobuild"
    else
        echo $lxprog error: unrecognized argument \"$1\"
        exit 1
    fi
elif [ $# -eq 0 ]; then
    "$UXDICE/setup/setup"
else
    echo $lxprog error: arg count
    exit 1
fi
echo ----------------------------
echo Setup of DICE is complete

popd
