This directory provides files for bootstrapping of the DICE setup.
===============================================================================
Here is the instructions for DICE setup:

To set up (install) the DICE software, please follow the following
steps in a Bash shell. The setup process will install DICE from the
dicepub repository.

1. Change your working directory to dice directory in the dicepub repository.

2. Execute the following command to install the software:

source setup/runme

3. The command above will by default create a dspcad_user directory directly
within your home directory. The dice_user directory are in the dspcad_user 
directory. 

4. If you have your customized dice_user directory for DICE setup. Please set 
the following variable before you execute the command for setting up DICE. 

export DXUSER=<path to dice_user directory>

