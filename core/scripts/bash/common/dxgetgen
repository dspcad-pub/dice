#!/usr/bin/env bash

# @ddblock_begin copyright
#
# Copyright (c) 1997-2024
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
#
# Permission is hereby granted, without written agreement and without
# license or royalty fees, to use, copy, modify, and distribute this
# software and its documentation for any purpose, provided that the above
# copyright notice and the following two paragraphs appear in all copies
# of this software.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
#
# @ddblock_end copyright 

# dxgetgen will rename the current gen related directory of DICE or LIDE using 
# version and revision information
# usage: dxgetgen <LIDE or DICE>



# check the argument list
if [ $# -ne 1 ]; then
    echo ${0##*/} error: arg count
fi

lxitem="$1"

# get version and revison information

# Get file name
if [ "$lxitem" == "dice" ]; then
    filename="$UXDICE"/doc/version-short.txt
elif [ "$lxitem" == "lide" ]; then
        filename="$UXLIDE"/doc/version-short.txt
else 
    echo ${0##*/} error: wrong argument for software to switch
    exit 1
fi


while read -r line1; read -r line2;
do
lxversion="$line1"
lxrevision="$line2"
done < "$filename"

# Rename the related-gen directory
if [ "$lxitem" == "dice" ]; then
    olddirname="$DXUSER""/dxgen"
    newdirname="$olddirname""-""$lxversion""-""$lxrevision"
    if [ -d "$newdirname" ]; then 
        echo WARNING: "$newdirname" directory exists; it will be replaced.
    fi
    rm -rf "$newdirname"
    mv "$olddirname" "$newdirname"
    
    olddirname="$DXUSER""/dxmgen"
    newdirname="$olddirname""-""$lxversion""-""$lxrevision"
    if [ -d "$newdirname" ]; then 
        echo WARNING: "$newdirname" directory exists; it will be replaced.
    fi
    rm -rf "$newdirname"
    mv "$olddirname" "$newdirname"
    
    olddirname="$DXUSER""/lang/dlxgen"
    newdirname="$olddirname""-""$lxversion""-""$lxrevision"
    if [ -d "$newdirname" ]; then 
        echo WARNING: "$newdirname" directory exists; it will be replaced.
    fi
    rm -rf "$newdirname"
    mv "$olddirname" "$newdirname"
    
elif [ "$lxitem" == "lide" ]; then
    olddirname="$LIDEUSER""/lidegen"
    newdirname="$olddirname""-""$lxversion""-r""$lxrevision"
    if [ -d "$newdirname" ]; then 
        echo WARNING: "$newdirname" directory exists; it will be replaced.
    fi
    rm -rf "$newdirname"
    mv "$olddirname" "$newdirname"
else 
    echo ${0##*/} error: invalid choice --- \"$lxitem\"
fi
