#!/usr/bin/env bash

###############################################################################
# @ddblock_begin copyright 
#
# Copyright (c) 1997-2024
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
#
# Permission is hereby granted, without written agreement and without
# license or royalty fees, to use, copy, modify, and distribute this
# software and its documentation for any purpose, provided that the above
# copyright notice and the following two paragraphs appear in all copies
# of this software.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
#
# @ddblock_end copyright 
###############################################################################

# DICE utilities for directory navigation 

# Initialization of package variable
export dxidsave="$UXTMP/navigation-id-save.txt"

###############################################################################
# FUNCTIONS THAT PERTAIN TO DIRECTORY AND url NAVIGATION.
###############################################################################

# dlk <label>
# Link a given label to the current directory for convenient navigation 
function dlk {
    local outtxt=""
    local lxlabel="$1"

    if [[ ($# -ne 1) ]]; then
        echo dlk error: arg count
        return 1
    fi

    # Remove a script with the same name (presumably created by olk)
    # if it exists (otherwise, the script may take priority)
    if [ -f "$UXGO/$lxlabel" ]; then
        rm "$UXGO/$lxlabel"
    fi

    outtxt="$UXGO/$lxlabel.txt"
    pwd > $outtxt

    # Save the label as the most recent label that was linked
    echo "$lxlabel" > "$dxidsave"
}

# Change directory based on a label that has been linked previously by dlk.
function g {
    local infile
    local lxprog="$FUNCNAME"
    local lxlabel="$1"
    local lxdest=""
    local lxdiscard="$UXTMP/${lxprog}-discard.txt"

    if [ $# -ne 1 ]; then
        >&2 echo "$lxprog error: arg count"
        return 1
    fi

    infile="$UXGO/$1.txt"

    if ! [ -f $infile ]; then 
        >&2 echo "$lxprog error: undefined navigation label \"$lxlabel\""
        return 1
    fi 

    # Quote the path in case it contains spaces
    lxdest="`cat $infile`"
    cd "$lxdest" 2> "$lxdiscard"
    if [ $? -ne 0 ]; then
        >&2 echo "$lxprog error: unreachable navigation target \"$lxdest\""
        return 1
    fi
}

# ulk <label> <URL>
# Link a given label to a given URL for conventient browser-based navigation.
function ulk {
    local outtxt
    local lxprog=ulk

    if [ $# -ne 2 ]; then
        echo $lxprog error: arg count
        return 1
    fi

    outtxt=$UXGU/$1.txt
    echo $2 > $outtxt
}

# @ddblock_begin doc_gu
# Browse to a URL based on a label has been linked previously by ulk.
# @ddblock_end doc_gu
function gu
{
    local lxinfile=""
    local lxtarget=""
    local lxprog="$FUNCNAME"

    lxinfile="$UXGU"/"$1".txt
    if ! [ -f "$lxinfile" ]; then 
        >&2 "echo $lxprog Error: $lxinfile does not exist"
        return 1
    fi 

    result=`type uxbrowse | grep function`
    if [ -z "$result" ]; then
        >&2 echo "$lxprog error: uxbrowse function is not defined"
        return 1
    fi
    lxtarget=`cat $lxinfile`
    uxbrowse "$lxtarget"
    if [ $? -ne 0 ]; then
        >&2 echo "$lxprog: error browsing with target = \"$lxtarget\""
        return 1
    fi
}

# Navigate to a directory using the directory stack
function plk {
    dxpushd .
    g $1
}

# Unlink (remove) a quick-navigation directory label. Labels created
# by both \ddcode{dlk} and \ddcode{olk} will be removed.
function rlk {
    rm -f $UXGO/$1.txt
    rm -f $UXGO/$1
}

# Unlink (remove) a quick-navigation URL label
function rulk {
    rm -f $UXGU/$1.txt
}

#
# Usage: dxpwd <name>
#
# Display the path of the current working directory relative to the 
# closest ancestor
# directory (lowest level parent) with the given name <name>. 
# . If the current working directory is called <name>, then display
# ".". Otherwise, if no ancestor directory is called <name>, then display
# the empty string (""). As a side effect, the name \ddcode{dxpwdbase}
# is linked (using \ddcode{dlk}) to the lowest level ancestor directory 
# named <name>. If no matching ancestor directory is found, then
# the link name \ddcode{dxpwdbase} is cleared using \ddcode{rlk}.
#
# Example: Suppose that we are in \ddcode{/homes/mickey/d1/d2/d3}, and we
# invoke \ddcode{dxpwd mickey}, then the output will be \ddcode{d1/d2/d3}.
# dxpathvar.txt, and suppose that we run \ddcode{dxpwd} from
# /homes/mickey/projects/proj1. Then the output will be:
# \ddcode{projects/proj1}, and we can subsequently go to the directory 
# \ddcode{/homes/mickey} by running \ddcode{g dxpwdbase}.
#
function dxpwd {
    local lxprog=dxpwd
    local lxpwd=`pwd`
    local lxdir=`basename "$lxpwd"`
    if [ $# -ne 1 ]; then
        echo $lxprog error: arg count
        return 1
    fi

    # Place this test first in case the argument is "/", which means that
    # the path relative to the root directory should be displayed.
    if [ "$lxdir" = "$1" ]; then
        echo "." 
        dlk dxpwdbase
    elif [ "$lxpwd" = "/" ]; then
        # Don't output anything
        echo -n ""
        rlk dxpwdbase
    else
        dxpushd ..
        local parent_result=`$lxprog "$1"`
        if [ "$parent_result" = "" ]; then
            result=""
        elif [ "$parent_result" = "." ]; then
            result="$lxdir"
        else
            result="$parent_result"/"$lxdir"
        fi
        if [ -n "$result" ]; then
            echo $result
        fi
        dxpopd
    fi
}

# Display the path of the current working directory relative to the 
# closest ancestor
# directory (lowest level parent) that contains a config/dxpathvar.txt
# file. If no ancestor directory contains such a file, then just
# display the full path for the current working directory.
#
# Example: Suppose that /homes/mickey/config contains a file called
# dxpathvar.txt, and suppose that we run \ddcode{dxpwd} from
# /homes/mickey/projects/proj1. Then the output will be:
# \ddcode{projects/proj1}.
#
# Usage: dxpwd <no arguments>

# olk <label> <base>
# DICE utility for pOrtable LinKing of directory navigation labels.
# Link a given label (through a generated script) to the current directory for 
# convenient navigation. The destination is relative to the path
# pointed to by an environment variable. The name of this environment
# variable is taken from the file <basedir>/dxconfig/olkvar.txt, where
# \ddcode{<basedir>} is the lowest level ancestor directory (of the
# current working directory) whose name is <base>.
#
# For example, suppose we are in /homes/donald/d1/d2/d3;
# there is a file /homes/donald/dxconfig/olkvar.txt whose contents
# is the string \ddcode{MY_HOME_BASE}; and we run \ddcode{olk current donald}. 
# Then a script will be created that navigates (changes the working
# directory) to \ddcode{$MY_HOME_BASE/d1/d2/d3}. This script can be
# invoked any time by running \ddcode{g current}. 
#
# This utility is useful for creating directory links that are
# used on different platforms or user accounts where the
# relevant directory structures are mirrord relative to some common
# base directory.
function olk {
    local lxprog=olk
    local var2
    local varfile=dxconfig/olkvar.txt
    local outscript="$UXGO/$1"

    if [[ ($# -ne 2) ]]; then
        echo $lxprog error: arg count
        return 1
    fi

    local var1=`dxpwd "$2"` 
    plk dxpwdbase
    if [ $? -ne 0 ]; then
        echo $lxprog error: error searching for base directory
        return 1
    fi

    if ! [ -f $varfile ]; then
        echo $lxprog error: Could not find $varfile file 
        dxpopd
        return 1
    fi
    var2=`cat $varfile`

    # Quote the target in case it contains spaces. 
    # The generated script needs to be sourced.
    echo \#!/usr/bin/env bash > "$outscript"
    echo >> "$outscript"
    echo \# Automatically generated script >> "$outscript"
    echo \# Generated by DICE olk on `date` >> "$outscript"
    echo >> "$outscript" 
    echo cd \"\$$var2/$var1\" >> "$outscript"

    dxpopd
}


# Display the directory that is referenced by
# the given navigation link.
# If the link does not exist, then display nothing, and return 1.
#
# Usage: dxshowlk <mode> <link>
#
# If the \ddcode{mode} argument is \ddcode{string}, then the string 
# that is used to 
# represent the destination directory
# is displayed on standard output; otherwise, if the
# \ddcode{mode} argument is \ddcode{path}, then the actual
# directory path that corresponds to the link is displayed.
# The former (the output from the \ddcode{string} mode) is the 
# destination directory as it appears 
# in the text file generated by \ddcode{dlk}.
# The mode specifier (first argument) is {\em case sensitive}.
#
# This script also serves as a test for whether or not a link
# is valid. If a link is not valid, then nothing will be displayed
# on standard output, and the function will return 1 (without
# any error message displayed).
function dxshowlk {
    local lxprog=dxshowlk
    if [ $# -ne 2 ]; then 
        echo $lxprog error: arg count
        return 1
    fi
    local lxmode="$1"
    local lxlink="$2"

    # Test the link, discarding any error message
    dxpushd .
    g "$lxlink" > "$UXTMP"/$lxprog-discard1.txt 2> "$UXTMP"/$lxprog-discard2.txt
    if [ $? -ne 0 ]; then
        return 1
    fi
    local lxpath="`pwd`"
    dxpopd

    if [ "$lxmode" = "path" ]; then
        echo "$lxpath"
    elif [ "$lxmode" = string ]; then
        infile="$UXGO"/$lxlink.txt

        if [ -f "$infile" ]; then
            cat "$infile"
        else
            return 1
        fi
    else
        echo $lxprog error: invalid mode specifier `$lxmode`
        return 1
    fi        
}


