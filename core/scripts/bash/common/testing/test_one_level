#!/usr/bin/env bash

# @ddblock_begin copyright 
#
# Copyright (c) 1997-2018
# Maryland DSPCAD Research Group, The University of Maryland at College Park 
# All rights reserved.

# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# 
# @ddblock_end copyright 

#
# Script to run the tests for one level of a dice-format testing tree.
# If there is a runme script in the present directory, then the
# the script assumes that this is a leaf level of the testing tree
# and executes the unit test in the current directory.
# Otherwise, the
# the script recursively traverses all sub-directories that begin with "test".
#
# Usage: test-one-level <no arguments>
#

export -a

if [ $run_tests_verbose = yes ]; then
    echo Running tests for directory `pwd`
fi

# Traverse all test directories and execute the tests

if [ -f runme ]; then 
    # Remove this use of runme to detect an ITS when things are stable with
    # the use of dxtest-desc.
    run_one_test
elif [ -f "dxtest-desc.txt" ]; then
    run_one_test
else
    test_subdir_found=false
    for file in `ls -d * 2>/dev/null` ; do
	    [ ! -d "$file" ] && continue
        if [[ "$file" =~ ^test.* ]] || [[ -f  "$file"/dxtest-dir.txt ]]; then
            test_subdir_found=true
            cd "$file"
            test_one_level
            cd ..
        fi
    done
    if [ $test_subdir_found = false ]; then
        echo Warning: No runme script or test subdirectory in `pwd`
    fi
fi

if [ $run_tests_verbose = yes ]; then
    echo Tests for directory `pwd` are complete.
fi

cd ..
